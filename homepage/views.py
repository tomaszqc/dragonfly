from django.shortcuts import render,reverse
from django.http import HttpResponse,HttpResponseRedirect
from django import forms
from users.models import UserProfile
from records.models import Record
from django.contrib.auth import authenticate, login,logout
from django.contrib.auth.decorators import login_required

import datetime

def index(request):
    if request.POST:
        form = Form_login(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                if request.GET.get('next') is not None:
                    return redirect(request.GET['next'])
        else:
            return render(request, "homepage/index.html", {'form' : form})
    else:
        form = Form_login()
    return render(request, "homepage/index.html", {'form' : form})

@login_required
def logoutUser(request):
    logout(request)
    return HttpResponseRedirect(reverse('homepage'))

class Form_login(forms.Form):
    username = forms.CharField(max_length=20,label="Login")
    password = forms.CharField(max_length=20,label="Hasło", widget=forms.PasswordInput)
    def clean(self):
        cleaned_data = super(Form_login,self).clean()
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        if not authenticate(username=username, password=password):
            raise forms.ValidateError("Zły login lub hasło")
        return self.cleaned_data
