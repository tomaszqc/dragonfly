from django.shortcuts import render

# Create your views here.
def indexPage(request):
    if request.is_ajax:
        dane=request.GET.get('term','')
    context={'imiona':['Kowalska','Bogdan','Zajac']}
    return render(request,'testapp/index.html',context)
