# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-02-14 22:11
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('records', '0004_auto_20170213_2222'),
    ]

    operations = [
        migrations.AlterField(
            model_name='record',
            name='author',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
