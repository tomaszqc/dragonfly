from django.conf.urls import url
from . import views

app_name = 'records'
urlpatterns = [
    url(r'^gatunekac/$', views.GatunekAutocomplete.as_view(), name='gatunekac'),
    #url(r'^view/(?P<recordId>[0-9]+)', views.viewRecord, name="viewRecord"),
    url(r'^main/$', views.homeRecords, name='homeRecords' ),
    url(r'^new/$', views.RecordAdd.as_view(), name='addRecord'),
    url(r'^view/(?P<pk>[0-9]+)', views.RecordDetailView.as_view(), name="viewRecord"),
    url(r'^browse/$', views.RecordListView.as_view(), name='browseRecords' ),
    url(r'^edit/(?P<recordId>[0-9]+)', views.editRecord, name='editRecord'),
]
