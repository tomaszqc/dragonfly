from users.models import User
from django.http import JsonResponse
from .models import Record,Gatunek
from records.models import UserProfile
from django import forms
from django.shortcuts import render,get_object_or_404,redirect
from django.contrib import messages
from django.http import Http404
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.forms import ModelForm
from dal import autocomplete
from django.views.generic import ListView,DetailView
from django.views.generic import CreateView,UpdateView


from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
import datetime


def homeRecords(request):
    return render(request,'records/homeRecords.html')


@login_required
def addRecord(request):
    error=False
    if request.POST:
        form = addRecordForm(request.POST)
        if form.is_valid():
            record_data=form.cleaned_data['record_data']
            species = form.cleaned_data['species']
            notes = form.cleaned_data['notes']
            habitat = form.cleaned_data['habitat']
            behaviour = form.cleaned_data['behaviour']
            county = form.cleaned_data['county']
            place = form.cleaned_data['place']
            num_individuals = form.cleaned_data['num_individuals']
            num_adults = form.cleaned_data['num_adults']
            num_juveniles = form.cleaned_data['num_juveniles']
            num_males = form.cleaned_data['num_males']
            num_females = form.cleaned_data['num_females']
            new_record = Record(author=request.user,record_data=record_data,species=species,notes=notes,habitat=habitat,behaviour=behaviour,county=county,place=place,num_individuals=num_individuals,num_adults=num_adults,num_juveniles=num_juveniles,num_males=num_males,num_females=num_females)
            new_record.save()
            messages.success(request, 'Pomyślnie dodano nowy rekord')
            return render(request,'records/addRecord.html',{'form':form,})
        else:
            messages.error(request,'wystąpiły błędy')
            return render(request,'records/addRecord.html',{'form':form,})

    else:
        form = addRecordForm
        return render(request,'records/addRecord.html',{'form': form})

class addRecordForm(forms.Form):
    record_data = forms.DateField(label="Data obserwacji",initial=datetime.date.today,input_formats=["%d.%m.%Y","%d/%m/%Y"],widget=forms.DateInput(format="%d.%m.%Y"))
    author = UserProfile.id #FIXME powinno byc pole formularza a nie od razu wartosc?
    species = forms.ModelChoiceField(queryset=Gatunek.objects.all(),widget=autocomplete.ModelSelect2(url='records:gatunekac'))
    #species = forms.CharField(label="Gatunek",max_length=50)
    notes = forms.CharField(label="Uwagi",widget=forms.Textarea,max_length=500,required=False)
    habitat = forms.ChoiceField(label="Siedlisko",choices=Record.HABITAT_CHOICES)
    behaviour = forms.ChoiceField(label="Zachowanie",choices=Record.BEHAVIOUR_CHOICES)
    county = forms.CharField(label="Powiat",max_length=50)
    place = forms.CharField(label="Miasto",max_length=50)
    num_individuals=forms.IntegerField(label="Łączna liczba osobników",min_value=1,max_value=100000)
    num_adults=forms.IntegerField(required=False,label="Liczba os. dorosłych")
    num_juveniles=forms.IntegerField(required=False,label="Liczba os. młodych")
    num_males=forms.IntegerField(required=False,label="Liczba samców")
    num_females=forms.IntegerField(required=False,label="liczba samic")

    def clean(self):
        cleaned_data = super(addRecordForm,self).clean()
        return self.cleaned_data

    def clean_record_data(self):
        record_data=self.cleaned_data['record_data']
        if not (datetime.date(2017,1,1) <= record_data <= datetime.date.today()):
            dzisiaj=datetime.date.today().strftime('%d.%m.%Y')
            raise forms.ValidationError("Data nie zawiera sie w przedziale 1.1.2017-"+dzisiaj)
        return record_data


@login_required
def editRecord(request,recordId):
    instance = get_object_or_404(Record, id=recordId)
    form=editRecordForm(request.POST or None, instance = instance)
    if form.is_valid():
        form.save()
        messages.success(request, 'Rekord zapisany')
        return redirect('records:browseRecords')
    return render(request,'records/editRecord.html', {'form':form,'recordId':recordId})


class editRecordForm(forms.ModelForm):
    class Meta:
        model = Record
        fields = '__all__'

@login_required
def viewRecord(request,recordId):
    record = get_object_or_404(Record, id=recordId)
    record_author=record.author.username
    record_habitat=record.get_habitat_display()
    record_behaviour=record.get_behaviour_display()
    form=viewRecordForm(initial={'author':record_author,'habitat':record_habitat,'behaviour':record_behaviour},instance=record)
    return render(request,'records/viewRecord.html', {'form':form,})

class viewRecordForm(forms.ModelForm):
    class Meta:
        model = Record
        fields = ['record_data','author','species','notes','habitat','behaviour','county','place','num_individuals','num_adults','num_juveniles','num_males','num_females']

    record_data = forms.DateField(label="Data obserwacji",disabled=True)
    author = forms.CharField(label="Autor obserwacji", disabled=True)
    species = forms.CharField(label="Gatunek",disabled=True)
    notes = forms.CharField(label="Uwagi",disabled=True)
    habitat = forms.CharField(label="Siedlisko",disabled=True)
    behaviour = forms.CharField(label="Zachowanie",disabled=True)
    county = forms.CharField(label="Powiat",disabled=True)
    place = forms.CharField(label="Miasto",disabled=True)
    num_individuals=forms.IntegerField(label="Łączna liczba osobników",disabled=True)
    num_adults=forms.IntegerField(label="Liczba os. dorosłych",disabled=True)
    num_juveniles=forms.IntegerField(label="Liczba os. młodych",disabled=True)
    num_males=forms.IntegerField(required=False,label="Liczba samców",disabled=True)
    num_females=forms.IntegerField(required=False,label="liczba samic",disabled=True)

@login_required
def browseRecords(request):
    context = {
        'record_list' : Record.objects.all()[:10]
    }
    return render(request, 'records/browseRecords.html',context)

class GatunekAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        qs = Gatunek.objects.all()

        if self.q:
            qs = qs.filter(nazwa_pl__istartswith=self.q)
        return qs

#----------------------------- class-based-views
class addRecordFormMeta(ModelForm):
    species = forms.ModelChoiceField(queryset=Gatunek.objects.all(),widget=autocomplete.ModelSelect2(url='records:gatunekac'))

    class Meta:
        model = Record
        fields = '__all__'
        exclude = ('author',)
        labels = {
            'record_data' : 'Data obserwacji',
            'species' : 'Gatunek',
            'notes' : 'Uwagi',
            'habitat' : 'Siedlisko',
            'behaviour' : 'Zachowanie',
            'county' : 'Powiat',
            'place' : 'Miejscowość',
            'num_individuals' : 'Liczba osobników',
            'num_adults' : 'Liczba os. dorosłych',
            'num_juveniles' : 'Liczba os. młodych',
            'num_males' : 'Liczba samców',
            'num_females' : 'Liczba samic'
        }

    def clean(self):
        cleaned_data = super(addRecordFormMeta,self).clean()
        return cleaned_data

    def clean_record_data(self):
        record_data=self.cleaned_data['record_data']
        if not (datetime.date(2017,1,1) <= record_data <= datetime.date.today()):
            dzisiaj=datetime.date.today().strftime('%d.%m.%Y')
            raise forms.ValidationError("Data nie zawiera sie w przedziale 1.1.2017-"+dzisiaj)
        return record_data

class RecordAdd(CreateView):
    model = Record
    template_name = 'records/addRecord2.html'
    form_class=addRecordFormMeta

    def post(self,request):
        form = addRecordFormMeta(request.POST or None)
        if form.is_valid():
            #marker_lat=form.cleaned_data['marker_lat']
            #marker_lng=form.cleaned_data['marker_lng']
            record_data=form.cleaned_data['record_data']
            species = form.cleaned_data['species']
            notes = form.cleaned_data['notes']
            habitat = form.cleaned_data['habitat']
            behaviour = form.cleaned_data['behaviour']
            county = form.cleaned_data['county']
            place = form.cleaned_data['place']
            num_individuals = form.cleaned_data['num_individuals']
            num_adults = form.cleaned_data['num_adults']
            num_juveniles = form.cleaned_data['num_juveniles']
            num_males = form.cleaned_data['num_males']
            num_females = form.cleaned_data['num_females']
            new_record = Record(author=self.request.user,record_data=record_data,species=species,notes=notes,habitat=habitat,behaviour=behaviour,county=county,place=place,num_individuals=num_individuals,num_adults=num_adults,num_juveniles=num_juveniles,num_males=num_males,num_females=num_females)
            new_record.save()
            messages.success(request, 'Pomyślnie dodano nowy rekord')
            #print(marker_lng,marker_lat)
            newForm = addRecordFormMeta(initial={'record_data': record_data,"notes" : "brak uwag",'habitat':habitat,'county':county,'place':place})
            return render(request,'records/addRecord2.html',{'form':newForm,})
        else:
            messages.error(request,'wystąpiły błędy')
            return render(request,'records/addRecord2.html',{'form':form,})
        #TODO: dodać przekierowanie na stronę nowego rekordu z częścią pól pouzupełnianych danym z poprzednio dodanego rekordu


class RecordDetailView(DetailView):

    model = Record
    template_name = 'records/viewRecord2.html'

    def get_context_data(self, **kwargs):
        context = super(RecordDetailView,self).get_context_data(**kwargs)
        context['record']= get_object_or_404(Record, pk=self.kwargs.get('pk', None))
        return context

class RecordListView(ListView):

    model = Record
    template_name = 'records/browseRecords2.html'

    def get_queryset(self):
        return Record.objects.all().order_by('-record_data')[:10]

    def get_context_data(self, **kwargs):
        context = super(RecordListView, self).get_context_data(**kwargs)
        context['records_num'] = Record.objects.all().count()
        self.object_list = self.get_queryset()
        return context
