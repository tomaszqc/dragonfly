from __future__ import unicode_literals

from django.db import models
from users.models import UserProfile
from django.contrib.auth.models import User
import datetime

class Gatunek(models.Model):
    nazwa_pl=models.CharField(max_length=128)
    nazwa_la=models.CharField(max_length=128)

    def  __str__(self):
        return self.nazwa_pl

class Record(models.Model):
    HABITAT_CHOICES = (
        ('FO','las'),
        ('ZA','zadrzewienie'),
        ('LA','łąka'),
        ('PO','pole'),
        ('NI','nieużytek'),
        ('TZ','teren zurbanizowany'),
        ('RZ','rzeka'),
        ('ZW','zbiornik wodny'),
        ('MO','wybrzeże morskie'),
    )
    BEHAVIOUR_CHOICES = (
        ('ST','stacjonarny'),
        ('PR','przelotny'),
        ('KO','koczujący'),
    )
    record_data = models.DateField()
    entry_data = models.DateField(auto_now_add=True) #
    author = models.ForeignKey(User)
    species = models.ForeignKey(Gatunek)
    notes = models.CharField(blank=True,null=True,max_length=500,default='')
    habitat = models.CharField(choices = HABITAT_CHOICES,max_length=20)
    behaviour = models.CharField(choices = BEHAVIOUR_CHOICES,max_length=20)
    county = models.CharField(max_length=50)
    place = models.CharField(max_length=50)
    num_individuals = models.IntegerField()
    num_adults = models.IntegerField(blank=True,null=True,default=0)
    num_juveniles = models.IntegerField(blank=True,null=True,default=0)
    num_males = models.IntegerField(blank=True,null=True,default=0)
    num_females = models.IntegerField(blank=True,null=True,default=0)


    def __str__(self):
        return ""+self.record_data.strftime("%Y,%m,%d")+","+self.species+","+str(self.num_individuals)+","+self.author
