from django.shortcuts import render,redirect
from .models import UserProfile
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
#from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django import forms
from django.http import HttpResponseRedirect
from django.http import HttpResponse
import datetime

# Create your views here.
def loginUser(request):
    if request.POST:
        form = Form_login_user(request.POST) # a co jesli bys utworzyl forma bez request.posta jak ponizej?
        if form.is_valid():
            username = form.cleaned_data['user_name']
            password = form.cleaned_data['user_password']
            user = authenticate(username=username,password=password)
            if user: #mozna tez: if user is not None
                login(request, user)
                return HttpResponseRedirect('udalo sie zalogowac, gratulacje')
            else:
                return HttpResponse('nie udalo sie zalogowac')
        else:
            return HttpRespons('formularze niepoprawnie zwalidowany')

            #wykombinuj bardziej ambitne rozwiazanie
            #render(request,'users/loginUser', {'message' : 'login error'})
        #moje kombinowane rozwiazanie niedokonczone z powodu natkniecia sie na dokumentacji metody login() i wygodniejszego rozwiazania zaprezentowanego w https://docs.djangoproject.com/en/1.10/topics/auth/default/
        #user_name = form.cleaned_data['user_name']
        # user_password = form.cleaned_data['user_password']
        # try:
        #     user = User.objects.get(username=user_name) # za duzo mieszasz z nazewnictwem zmiennych, ujednolic to
        # except User.DoesNotExist:
        #     raise forms.ValidationError('bledna nazwa uzytkownika')
        # if(user.password == user_password):
        #     #autoryzacja
        # else:
        #     forms.ValidationError('bledne haslo')


    else:
        form = Form_login_user()
        return render(request,'users/loginUser.html', {'form' : form })

def createUser(request):
    if request.POST:
        form = Form_add_user(request.POST)
        if form.is_valid():
                username = form.cleaned_data['username']
                name = form.cleaned_data['name']
                surname = form.cleaned_data['surname']
                email = form.cleaned_data['email']
                password = form.cleaned_data['password']
                new_user = User.objects.create_user(username = username,first_name = name,last_name = surname, email = email, password=password)
                new_user.is_active = True
                new_user.last_name = name
                new_user.save()
                new_UserProfile=UserProfile(user_auth=new_user,registration_date=datetime.datetime.now().strftime("%Y-%m-%d"))
                new_UserProfile.save()
                messages.success(request, 'Pomyślnie utworzono nowego użytkownika,mozesz sie zalogować')
                return render(request,'users/createUser.html', {'form' : form})
                #nie bedziemy na razie bawic sie w funkcje reverse
                #return HttpResponseRedirect(reverse('public_empty'))
        else:
            return render(request,'users/createUser.html', {'form' : form})
    else:
        form = Form_add_user()
        return render(request,'users/createUser.html', {'form' : form})

class Form_login_user(forms.Form):
    user_name = forms.CharField(label="username",max_length=30)
    user_password = forms.CharField(label="password")

class Form_add_user(forms.Form):
    name = forms.CharField(label="Imie",max_length=30)
    surname = forms.CharField(label="Nazwisko",max_length=30 )
    username = forms.CharField(label="Login",max_length=30)
    email = forms.EmailField(label="Email",max_length=30)
    password = forms.CharField(label="haslo", widget=forms.PasswordInput,max_length=30)
    password_bis = forms.CharField(label="Powtorz haslo", widget=forms.PasswordInput,max_length=30)
    def clean(self):
        cleaned_data= super (Form_add_user, self).clean()
        password = self.cleaned_data.get('password')
        password_bis = self.cleaned_data.get('password_bis')
        if(password != password_bis):
            raise forms.ValidationError('hasla nie sa identyczne')
        return self.cleaned_data
