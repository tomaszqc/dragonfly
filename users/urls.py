from django.conf.urls import url, include
from django.contrib import admin
from . import views

app_name='users'
urlpatterns = [
    url(r'^create/$', views.createUser, name='createUser' ),
    url(r'^login/$', views.loginUser, name='loginUser'),
    ]
