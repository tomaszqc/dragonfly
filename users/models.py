from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    user_auth = models.OneToOneField(User, models.CASCADE)
    location = models.TextField(max_length=100, blank=True)
    registration_date = models.DateField(verbose_name="Data rejestracji")
    last_connected = models.DateTimeField(verbose_name="Data ostatniego logowania", null=True, default=None, blank=True)

    def __str__(self):
        return self.user_auth.username
